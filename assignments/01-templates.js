var gulp   = require('gulp'),
    gutil  = require('gulp-util'),
    S      = require('highland'),
    expect = require('expect.js'),
    path   = require('path'),
    fs     = require('fs'),
    _      = require('lodash'),
    del    = require('del');


describe('Compiling templates', function() {
    it('01: starts with by first getting the template files', function(done) {
        var templates = S(
            gulp
                .src('./templates/*.jst')
        );
        templates
            .map(function(file) {
                // How do you get the filename?
                // https://github.com/wearefractal/vinyl
                // http://nodejs.org/api/path.html
                return file.base;
            })
            .toArray(function(listOfTemplates) {
                expect(listOfTemplates).to.be.eql([
                    'color.jst',
                    'hello.jst'
                ]);
                done();
            });
    });

    it('02: then goes on to looking at the contents', function(done) {
        var templates = S(
            gulp
                .src('./templates/*.jst')
        );
        templates
            .map(function(file) {
                // How do you get the contents?
                // https://github.com/wearefractal/vinyl
                return file.base;
            })
            .toArray(function(listOfTemplates) {
                var colors =
                        fs.readFileSync('./templates/color.jst',
                                        {encoding: 'utf8'}),
                    hello =
                        fs.readFileSync('./templates/hello.jst',
                                        {encoding: 'utf8'});
                expect(listOfTemplates).to.be.eql([
                    colors,
                    hello
                ]);
                done();
            });
    });
    it('03: then goes on to using the contents with _.template', function(done) {
        var templates = S(
            gulp
                .src('./templates/*.jst')
        );
        templates
            .map(function(file) {
                // Compile it to a js string
                // https://lodash.com/docs#template
                return file.base;
            })
            .toArray(function(listOfTemplates) {
                var colorsCompiled =
                        "function (obj) {\n" +
                        "obj || (obj = {});\n" +
                        "var __t, __p = '', __e = _.escape;\n" +
                        "with (obj) {\n" +
                        "__p += '<span class=\"color ' +\n" +
                        "((__t = (color)) == null ? '' : __t) +\n" +
                        "'\"></span>\\n" +
                        "';\n"+
                        "\n" +
                        "}\n" +
                        "return __p\n"+
                        "}",
                    helloCompiled =
                        "function (obj) {\n" +
                        "obj || (obj = {});\n" +
                        "var __t, __p = '', __e = _.escape;\n" +
                        "with (obj) {\n" +
                        "__p += '<div class=\"hello\">\\n"+
                        "  <h1>Hello ' +\n" +
                        "((__t = (name)) == null ? '' : __t) +\n" +
                        "'</h1>\\n" +
                        "</div>\\n" +
                        "';\n" +
                        "\n" +
                        "}\n" +
                        "return __p\n" +
                        "}";
                expect(listOfTemplates).to.be.eql([
                    colorsCompiled,
                    helloCompiled
                ]);
                done();
            });
    });

    it('04: then goes on to using _.template output as base generating js module string', function(done) {
        var templates = S(
            gulp
                .src('./templates/*.jst')
        ),
            compiledTemplates =
                templates.map(function(file) {
                    // Compile it to a js string
                    // https://lodash.com/docs#template
                    return file.base;
                }),
            templateModuleStrings =
                compiledTemplates.map(function(compiledTemplate) {
                    return 'What to add here?' + compiledTemplate  + '...and here?';
                });
        templateModuleStrings.toArray(function(listOfTemplateModuleStrings) {
            var colorTemplateModule =
                    "module.exports = function (obj) {\n" +
                    "obj || (obj = {});\n" +
                    "var __t, __p = '', __e = _.escape;\n" +
                    "with (obj) {\n" +
                    "__p += '<span class=\"color ' +\n" +
                    "((__t = (color)) == null ? '' : __t) +\n" +
                    "'\"></span>\\n" +
                    "';\n"+
                    "\n" +
                    "}\n" +
                    "return __p\n"+
                    "};",
                helloTemplateModule =
                    "module.exports = function (obj) {\n" +
                    "obj || (obj = {});\n" +
                    "var __t, __p = '', __e = _.escape;\n" +
                    "with (obj) {\n" +
                    "__p += '<div class=\"hello\">\\n"+
                    "  <h1>Hello ' +\n" +
                    "((__t = (name)) == null ? '' : __t) +\n" +
                    "'</h1>\\n" +
                    "</div>\\n" +
                    "';\n" +
                    "\n" +
                    "}\n" +
                    "return __p\n" +
                    "};";

            expect(listOfTemplateModuleStrings).to.be.eql([
                colorTemplateModule,
                helloTemplateModule
            ]);
            done();
        });
    });

    function delTemplateJsFiles() {
        del(['./templates/*.js']);
    }

    beforeEach(delTemplateJsFiles);
    afterEach(delTemplateJsFiles);

    it('05: then goes on to write the generated output to a file', function(done) {
        var templates = S(
            gulp
                .src('./templates/*.jst')
        ),
            compiledTemplates =
                templates.map(function(file) {
                    // Compile it to a js string
                    // https://lodash.com/docs#template
                    return {
                        path: file.base,
                        compiled: file.base
                    };
                }),
            templateModuleStrings =
                compiledTemplates.map(function(compiledTemplate) {
                    return {
                        path: compiledTemplate.path,
                        module: 'What to add here... ' + compiledTemplate.compiled  + '...and here?'
                    };
                }),
            templateModuleFiles =
                templateModuleStrings.map(function(moduleString) {
                    // https://github.com/wearefractal/vinyl
                    // https://github.com/gulpjs/gulp-util#new-fileobj
                    return new gutil.File({
                        base: path.dirname(moduleString.path),
                        cwd: __dirname,
                        path: gutil.replaceExtension(moduleString.path, '.js')
                    });
                }),
            writtenFiles =
                templateModuleFiles.through(gulp.dest('./templates'));

        writtenFiles.apply(function(colorTemplateModuleFile,
                                    helloTemplateModuleFile) {
            var colorTemplateModule =
                    "module.exports = function (obj) {\n" +
                    "obj || (obj = {});\n" +
                    "var __t, __p = '', __e = _.escape;\n" +
                    "with (obj) {\n" +
                    "__p += '<span class=\"color ' +\n" +
                    "((__t = (color)) == null ? '' : __t) +\n" +
                    "'\"></span>\\n" +
                    "';\n"+
                    "\n" +
                    "}\n" +
                    "return __p\n"+
                    "};",
                helloTemplateModule =
                    "module.exports = function (obj) {\n" +
                    "obj || (obj = {});\n" +
                    "var __t, __p = '', __e = _.escape;\n" +
                    "with (obj) {\n" +
                    "__p += '<div class=\"hello\">\\n"+
                    "  <h1>Hello ' +\n" +
                    "((__t = (name)) == null ? '' : __t) +\n" +
                    "'</h1>\\n" +
                    "</div>\\n" +
                    "';\n" +
                    "\n" +
                    "}\n" +
                    "return __p\n" +
                    "};",
                readColorFile =
                    fs.readFileSync(colorTemplateModuleFile.path,
                                    {encoding: 'utf8'}),
                readHelloFile =
                    fs.readFileSync(helloTemplateModuleFile.path,
                                    {encoding: 'utf8'});


            expect(readColorFile).to.be.eql(colorTemplateModule);
            expect(readHelloFile).to.be.eql(helloTemplateModule);
            done();
        });
    });

    it('06: to compile only hello template file', function(done) {
        var templates = S(
            gulp
                .src('./templates/*.jst')
        ),
            filteredFilesTemplateFiles =
                templates.filter(function(file) {
                    // How do you get only hello.jst file?
                    // http://highlandjs.org/#filter
                    return true;
                }),
            compiledTemplates =
                filteredFilesTemplateFiles.map(function(file) {
                    // Compile it to a js string
                    // https://lodash.com/docs#template
                    return {
                        path: file.base,
                        compiled: file.base
                    };
                }),
            templateModuleStrings =
                compiledTemplates.map(function(compiledTemplate) {
                    return {
                        path: compiledTemplate.path,
                        module: 'What to add here... ' + compiledTemplate.compiled  + '...and here?'
                    };
                }),
            templateModuleFiles =
                templateModuleStrings.map(function(moduleString) {
                    // https://github.com/wearefractal/vinyl
                    // https://github.com/gulpjs/gulp-util#new-fileobj
                    return new gutil.File({
                        base: path.dirname(moduleString.path),
                        cwd: __dirname,
                        path: gutil.replaceExtension(moduleString.path, '.js')
                    });
                }),
            writtenFiles =
                templateModuleFiles.through(gulp.dest('./templates'));

        writtenFiles.apply(function(helloTemplateModuleFile) {
            var helloTemplateModule =
                    "module.exports = function (obj) {\n" +
                    "obj || (obj = {});\n" +
                    "var __t, __p = '', __e = _.escape;\n" +
                    "with (obj) {\n" +
                    "__p += '<div class=\"hello\">\\n"+
                    "  <h1>Hello ' +\n" +
                    "((__t = (name)) == null ? '' : __t) +\n" +
                    "'</h1>\\n" +
                    "</div>\\n" +
                    "';\n" +
                    "\n" +
                    "}\n" +
                    "return __p\n" +
                    "};",
                readHelloFile =
                    fs.readFileSync(helloTemplateModuleFile.path,
                                    {encoding: 'utf8'});

            expect(readHelloFile).to.be.eql(helloTemplateModule);
            done();
        });
    });
});
