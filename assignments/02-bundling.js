var gulp   = require('gulp'),
    gutil  = require('gulp-util'),
    bower  = require('main-bower-files'),
    S      = require('highland'),
    expect = require('expect.js'),
    path   = require('path'),
    fs     = require('fs'),
    _      = require('lodash'),
    del    = require('del');

describe('Bundling files', function() {
    it('01: can begin from combining files together', function() {
        S(
            bower({
                checkExistence: true
            })
        )
            .map(function(file) {
                return file;
            })
            .toArray(function(bundle) {
                var combined = bundle.join(''),
                    reference = fs.readFileSync('./bundles/reference-bundle.js',
                                                {encoding: 'utf8'});
                expect(combined).to.be.eql(reference);
            });
    });
});
